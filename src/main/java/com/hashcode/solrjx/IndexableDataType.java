/*
 * Created on Oct 3, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.solrjx;

/**
 * <p>
 * Data Type for the Indexable properties.
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public enum IndexableDataType {

	INT("_i"), LONG("_l"), DOUBLE("_d"), FLOAT("_f"), BOOLEAN("_b"), DATE("_dt"), TEXT("_t"), STRING("_s"), 
	LOCATION("_p"), COORDINATES("_coordinate"), DEFAULT("");

	String solrTypeSuffix;

	/**
	 * <p>
	 * Constructor 
	 * </p>
	 *
	 * @param suffix
	 */
	IndexableDataType(String suffix) {
		solrTypeSuffix = suffix;
	}

	/**
	 * <p>
	 * Getter method for solrTypeSuffix
	 * </p>
	 * 
	 * @return the solrTypeSuffix
	 */
	public String getSolrTypeSuffix() {
		return solrTypeSuffix;
	}

}
