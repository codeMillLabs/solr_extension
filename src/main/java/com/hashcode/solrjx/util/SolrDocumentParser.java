/*
 * Created on Oct 3, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.solrjx.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.solrjx.Indexable;

/**
 * <p>
 * SolrDocumentParser
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public class SolrDocumentParser {
	
    private static final Logger log = LoggerFactory.getLogger(SolrDocumentParser.class);

	/**
	 * <p>
	 * Parse the given java bean objects with Indexable to SolrInputDocuments.
	 * </p>
	 *
	 * @param docs
	 *            java beans for indexing
	 * @return Collection of {@link SolrInputDocument}
	 * @throws SolrParserException
	 *             throws at any error
	 */
	public static <T> List<SolrInputDocument> parseDocs(List<T> docs)
			throws SolrParserException {
		List<SolrInputDocument> docList = new ArrayList<SolrInputDocument>();
		for (T doc : docs) {
			SolrInputDocument inputDoc = parseDoc(doc);
			docList.add(inputDoc);
		}
		log.debug("SolrDocumentParser parsed {} docs for indexing", docList.size());
		return docList;
	}

	/**
	 * <p>
	 * Parse the given java bean object with Indexable to SolrInputDocument.
	 * </p>
	 *
	 * @param doc
	 *            java bean for indexing
	 * @return {@link SolrInputDocument}
	 * @throws SolrParserException
	 *             throws at any error
	 */
	private static <T> SolrInputDocument parseDoc(T doc)
			throws SolrParserException {
		SolrInputDocument inputDoc = new SolrInputDocument();
		
		// add fields from Field annotations
		fieldAnnotaion(doc, inputDoc);

		// add fields from Method annotations
		methodAnnotaion(doc, inputDoc);

		return inputDoc;
	}

	/**
	 * <p>
	 * Add the field values for fields which has Indexable annotation
	 * </p>
	 *
	 * @param doc
	 *            bean object
	 * @param inputDoc
	 *            {@link SolrInputDocument}
	 * @throws SolrParserException
	 *             throws at any error
	 */
	private static <T> void fieldAnnotaion(T doc, SolrInputDocument inputDoc)
			throws SolrParserException {
		for (Field field : doc.getClass().getDeclaredFields()) {
			try {
				field.setAccessible(true); // to access private variables
				Indexable annotation = field.getAnnotation(Indexable.class);

				if (annotation != null) {
					String fieldName = getFieldName(annotation);

					Object value = field.get(doc);
					log.debug("fieldAnnotaion, field name :{}, solr field : {}, value: {} ", field.getName(), fieldName, value);
					inputDoc.addField(fieldName, value);
				}
			} catch (Exception e) {
				throw new SolrParserException(
						"Error encountered in accessing the field, [ Field name :"
								+ field.getName() + "] ", e);
			}

		}
	}

	/**
	 * <p>
	 * Add the field values for methods which has Indexable annotation
	 * </p>
	 *
	 * @param doc
	 *            bean object
	 * @param inputDoc
	 *            {@link SolrInputDocument}
	 * @throws SolrParserException
	 *             throws in any error
	 */
	private static <T> void methodAnnotaion(T doc, SolrInputDocument inputDoc)
			throws SolrParserException {
		for (Method method : doc.getClass().getMethods()) {
			try {
				method.setAccessible(true); // to access private variables
				Indexable annotation = method.getAnnotation(Indexable.class);

				if (annotation != null) {
					String fieldName = getFieldName(annotation);

					Object value = method.invoke(doc);
					log.debug("methodAnnotaion, method name :{}, solr field : {}, value: {} ", method.getName(), fieldName, value);
					inputDoc.addField(fieldName, value);
				}
			} catch (Exception e) {
				throw new SolrParserException(
						"Error encountered in accessing the method, [ method name :"
								+ method.getName() + "] ", e);
			}

		}
	}

	/**
	 * <p>
	 * Get solr schema compatible field name.
	 * </p>
	 *
	 * @param annotation
	 *            {@link Indexable}
	 * @return fieldName
	 */
	private static String getFieldName(Indexable annotation) {
		String fieldName;
		if (annotation.id()) {
			fieldName = "id";
		} else if (annotation.multivalue()) {
			fieldName = annotation.fieldName()
					+ annotation.type().getSolrTypeSuffix() + "s";
		} else {
			fieldName = annotation.fieldName()
					+ annotation.type().getSolrTypeSuffix();
		}
		return fieldName;
	}

}
