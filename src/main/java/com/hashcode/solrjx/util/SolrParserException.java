/*
 * Created on Oct 3, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.solrjx.util;

/**
 * <p>
 *  SolrParserException
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public class SolrParserException extends Exception {

	private static final long serialVersionUID = -6138379299723401837L;

	public SolrParserException() {
		super();
	}

	public SolrParserException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SolrParserException(String message, Throwable cause) {
		super(message, cause);
	}

	public SolrParserException(String message) {
		super(message);
	}

	public SolrParserException(Throwable cause) {
		super(cause);
	}

}
