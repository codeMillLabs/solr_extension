/*
 * Created on Oct 3, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.solrjx;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Annotation provides the features to annotate indexable properties or methods.
 * </p>
 *
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface Indexable {

	/**
	 * <p>
	 * Solr index file field name.
	 * </p>
	 * 
	 */
	String fieldName();

	/**
	 * <p>
	 * Indicates the id field of the solr index
	 * </p>
	 *
	 * @return true if id field otherwise false;
	 */
	boolean id() default false;

	/**
	 * <p>
	 * Indicates the field contains multi value data for solr
	 * </p>
	 *
	 * @return true if multi value field otherwise false;
	 */
	boolean multivalue() default false;

	/**
	 * <p>
	 * Indicates the data type for the dynamic fields in solr index.
	 * </p>
	 *
	 * @return {@link IndexableDataType }
	 */
	IndexableDataType type() default IndexableDataType.DEFAULT;

}
