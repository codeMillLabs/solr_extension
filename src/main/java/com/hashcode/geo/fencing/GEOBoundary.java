package com.hashcode.geo.fencing;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * <p>
 * GEOBoundary polygon create a virtual boundary from the coordinates
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public class GEOBoundary {
	private final BoundingBox boundingBox;
	private final List<Line> sides;

	private GEOBoundary(List<Line> sides, BoundingBox boundingBox) {
		this.sides = sides;
		this.boundingBox = boundingBox;
	}

	/**
	 * Get the builder of the GEOBoundary polygon
	 * 
	 * @return The builder
	 */
	public static Builder Builder() {
		return new Builder();
	}

	/**
	 * Builder of the polygon
	 * 
	 */
	public static class Builder {
		private List<Coordinate> vertexes = new ArrayList<Coordinate>();
		private List<Line> sides = new ArrayList<Line>();
		private BoundingBox boundingBox = null;

		private boolean firstPoint = true;
		private boolean isClosed = false;

		/**
		 * Add vertex points of the polygon.<br>
		 * It is very important to add the vertexes by order, like you were
		 * drawing them one by one.
		 * 
		 * @param point
		 *            The vertex point
		 * @return The builder
		 */
		public Builder addVertex(Coordinate point) {
			if (isClosed) {
				// each hole we start with the new array of vertex points
				vertexes = new ArrayList<Coordinate>();
				isClosed = false;
			}

			updateBoundingBox(point);
			vertexes.add(point);

			// add line (edge) to the polygon
			if (vertexes.size() > 1) {
				Line Line = new Line(vertexes.get(vertexes.size() - 2), point);
				sides.add(Line);
			}

			return this;
		}

		/**
		 * Close the polygon shape. This will create a new side (edge) from the
		 * <b>last</b> vertex point to the <b>first</b> vertex point.
		 * 
		 * @return The builder
		 */
		public Builder close() {
			validate();

			// add last Line
			sides.add(new Line(vertexes.get(vertexes.size() - 1), vertexes
					.get(0)));
			isClosed = true;

			return this;
		}

		/**
		 * Build the instance of the polygon shape.
		 * 
		 * @return The polygon
		 */
		public GEOBoundary build() {
			validate();

			// in case you forgot to close
			if (!isClosed) {
				// add last Line
				sides.add(new Line(vertexes.get(vertexes.size() - 1), vertexes
						.get(0)));
			}

			GEOBoundary geoBoundary = new GEOBoundary(sides, boundingBox);
			return geoBoundary;
		}

		/**
		 * Update bounding box with a new point.<br>
		 * 
		 * @param point
		 *            New point
		 */
		private void updateBoundingBox(Coordinate point) {
			if (firstPoint) {
				boundingBox = new BoundingBox();
				boundingBox.xMax = point.getLatitude();
				boundingBox.xMin = point.getLatitude();
				boundingBox.yMax = point.getLongitute();
				boundingBox.yMin = point.getLongitute();

				firstPoint = false;
			} else {
				// set bounding box
				if (point.getLatitude() > boundingBox.xMax) {
					boundingBox.xMax = point.getLatitude();
				} else if (point.getLatitude() < boundingBox.xMin) {
					boundingBox.xMin = point.getLatitude();
				}
				if (point.getLongitute() > boundingBox.yMax) {
					boundingBox.yMax = point.getLongitute();
				} else if (point.getLongitute() < boundingBox.yMin) {
					boundingBox.yMin = point.getLongitute();
				}
			}
		}

		private void validate() {
			if (vertexes.size() < 3) {
				throw new RuntimeException(
						"GEOBoundary must have at least 3 coordinates");
			}
		}
	}

	/**
	 * Check if the the given point is inside of the polygon.<br>
	 * 
	 * @param point
	 *            The point to check
	 * @return <code>True</code> if the point is inside the polygon, otherwise
	 *         return <code>False</code>
	 */
	public boolean contains(Coordinate point) {
		if (inBoundingBox(point)) {
			Line ray = createRay(point);
			int intersection = 0;
			for (Line side : sides) {
				if (intersect(ray, side)) {
					// System.out.println("intersection++");
					intersection++;
				}
			}

			/*
			 * If the number of intersections is odd, then the point is inside
			 * the polygon
			 */
			if (intersection % 2 == 1) {
				return true;
			}
		}
		return false;
	}

	public List<Line> getSides() {
		return sides;
	}

	/**
	 * By given ray and one side of the polygon, check if both lines intersect.
	 * 
	 * @param ray
	 * @param side
	 * @return <code>True</code> if both lines intersect, otherwise return
	 *         <code>False</code>
	 */
	private boolean intersect(Line ray, Line side) {
		Coordinate intersectPoint = null;

		// if both vectors aren't from the kind of x=1 lines then go into
		if (!ray.isVertical() && !side.isVertical()) {
			// check if both vectors are parallel. If they are parallel then no
			// intersection point will exist
			if (ray.getA() - side.getA() == 0) {
				return false;
			}

			double x = ((side.getB() - ray.getB()) / (ray.getA() - side.getA())); // x
																					// =
																					// (b2-b1)/(a1-a2)
			double y = side.getA() * x + side.getB(); // y = a2*x+b2
			intersectPoint = new Coordinate(x, y);
		}

		else if (ray.isVertical() && !side.isVertical()) {
			double x = ray.getStart().getLatitude();
			double y = side.getA() * x + side.getB();
			intersectPoint = new Coordinate(x, y);
		}

		else if (!ray.isVertical() && side.isVertical()) {
			double x = side.getStart().getLatitude();
			double y = ray.getA() * x + ray.getB();
			intersectPoint = new Coordinate(x, y);
		}

		else {
			return false;
		}

//		System.out.println("Ray: " + ray.toString() + " ,Side: " + side);
//		System.out.println("Intersect point: " + intersectPoint.toString());

		if (side.isInside(intersectPoint) && ray.isInside(intersectPoint)) {
			return true;
		}

		return false;
	}

	/**
	 * Create a ray. The ray will be created by given point and on point outside
	 * of the polygon.<br>
	 * The outside point is calculated automatically.
	 * 
	 * @param point
	 * @return
	 */
	private Line createRay(Coordinate point) {
		// create outside point
		double epsilon = (boundingBox.xMax - boundingBox.xMin) / 100d;
		Coordinate outsidePoint = new Coordinate(boundingBox.xMin - epsilon,
				boundingBox.yMin);

		Line vector = new Line(outsidePoint, point);
		return vector;
	}

	/**
	 * Check if the given point is in bounding box
	 * 
	 * @param point
	 * @return <code>True</code> if the point in bounding box, otherwise return
	 *         <code>False</code>
	 */
	private boolean inBoundingBox(Coordinate point) {
		if (point.getLatitude() < boundingBox.xMin
				|| point.getLatitude() > boundingBox.xMax
				|| point.getLongitute() < boundingBox.yMin
				|| point.getLongitute() > boundingBox.yMax) {
			return false;
		}
		return true;
	}

	private static class BoundingBox {
		public double xMax = Double.NEGATIVE_INFINITY;
		public double xMin = Double.NEGATIVE_INFINITY;
		public double yMax = Double.NEGATIVE_INFINITY;
		public double yMin = Double.NEGATIVE_INFINITY;
	}
}
