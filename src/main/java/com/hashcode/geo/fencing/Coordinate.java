package com.hashcode.geo.fencing;

/**
 * 
 * <p>
 * Coordinate on 2D landscape
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public class Coordinate {
	private double latitude;
	private double longitute;

	public Coordinate(double latitude, double longitute) {
		this.latitude = latitude;
		this.longitute = longitute;
	}

	/**
	 * <p>
	 * Getter method for latitude
	 * </p>
	 * 
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * <p>
	 * Setter method for latitude
	 * </p>
	 *
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * <p>
	 * Getter method for longitute
	 * </p>
	 * 
	 * @return the longitute
	 */
	public double getLongitute() {
		return longitute;
	}

	/**
	 * <p>
	 * Setter method for longitute
	 * </p>
	 *
	 * @param longitute
	 *            the longitute to set
	 */
	public void setLongitute(double longitute) {
		this.longitute = longitute;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("Coordinate [latitude=%s, longitute=%s]",
				latitude, longitute);
	}

}