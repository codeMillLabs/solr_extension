package com.hashcode.geo.fencing;


/**
 * 
 * <p>
 *  Straight Line
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public class Line
{
	private final Coordinate start;
	private final Coordinate end;
	private double a = Double.NaN;
	private double b = Double.NaN;
	private boolean vertical = false;

	public Line(Coordinate start, Coordinate end)
	{
		this.start = start;
		this.end = end;

		if (end.getLatitude() - start.getLatitude() != 0)
		{
			a = ((end.getLongitute() - start.getLongitute()) / (end.getLatitude() - start.getLatitude()));
			b = start.getLongitute() - a * start.getLatitude();
		}

		else
		{
			vertical = true;
		}
	}

	/**
	 * Indicate whereas the point lays on the line.
	 * 
	 * @param point
	 *            - The point to check
	 * @return <code>True</code> if the point lays on the line, otherwise return <code>False</code>
	 */
	public boolean isInside(Coordinate point)
	{
		double maxX = start.getLatitude() > end.getLatitude() ? start.getLatitude() : end.getLatitude();
		double minX = start.getLatitude() < end.getLatitude() ? start.getLatitude() : end.getLatitude();
		double maxY = start.getLongitute() > end.getLongitute() ? start.getLongitute() : end.getLongitute();
		double minY = start.getLongitute() < end.getLongitute() ? start.getLongitute() : end.getLongitute();

		if ((point.getLatitude() >= minX && point.getLatitude() <= maxX) && (point.getLongitute() >= minY && point.getLongitute() <= maxY))
		{
			return true;
		}
		return false;
	}

	/**
	 * Indicate whereas the line is vertical. <br>
	 * For example, line like x=1 is vertical, in other words parallel to axis Y. <br>
	 * In this case the A is (+/-)infinite.
	 * 
	 * @return <code>True</code> if the line is vertical, otherwise return <code>False</code>
	 */
	public boolean isVertical()
	{
		return vertical;
	}

	/**
	 * y = <b>A</b>x + B
	 * 
	 * @return The <b>A</b>
	 */
	public double getA()
	{
		return a;
	}

	/**
	 * y = Ax + <b>B</b>
	 * 
	 * @return The <b>B</b>
	 */
	public double getB()
	{
		return b;
	}

	/**
	 * Get start point
	 * 
	 * @return The start point
	 */
	public Coordinate getStart()
	{
		return start;
	}

	/**
	 * Get end point
	 * 
	 * @return The end point
	 */
	public Coordinate getEnd()
	{
		return end;
	}

	@Override
	public String toString()
	{
		return String.format("%s-%s", start.toString(), end.toString());
	}
}
