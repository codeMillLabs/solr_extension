package com.hashcode.solrjx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;

import com.hashcode.solrjx.beans.Center;
import com.hashcode.solrjx.beans.StockInfo;
import com.hashcode.solrjx.util.SolrDocumentParser;
import com.hashcode.solrjx.util.SolrParserException;


/**
 * <p>
 * Indexer Tester
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public class AppTest {

//	private static final String SOLR_SERVER_URL = "http://localhost:8983/solr/test_db";
	private static final String SOLR_SERVER_URL = "http://localhost:8983/solr/multi_1";

	private static SolrServer getConnection() throws Exception {

		SolrServer server = new HttpSolrServer(SOLR_SERVER_URL);
		System.out.println("::: SOLR Server connection "
				+ server.ping().getStatus());
		return server;
	}

	private static <T> void indexer(SolrServer server)
			throws SolrParserException, SolrServerException, IOException {

		System.out.println("::: indexer :::");

		List<T> testData = getTestData();

		List<SolrInputDocument> docs = SolrDocumentParser.parseDocs(testData);
		server.add(docs);
		server.commit();
		System.out.println("::: indexed ::: [ No items : " + docs.size() + "]");
	}

	public static void main(String[] a) throws Exception {
		SolrServer server = getConnection();
		indexer(server);

	}
	
	private static <T> List<T> getTestData() {

		Center center1 = new Center(101L, "NBC", "NBC-01", "My Home-Amila", 6.849459823898128,  79.87345397472382);
		Center center2 = new Center(102L, "NBC", "NBC-01", "Dehiwela-J", 6.851334618603244, 79.86600816249847);
		Center center3 = new Center(103L, "NBC", "NBC-01", "Wellawatha", 6.870849089086742, 79.86202776432037);
		Center center4 = new Center(104L, "NBC", "NBC-01", "Allan Ave", 6.859835015729054, 79.86713469028473);
		Center center5 = new Center(105L, "NBC", "NBC-01", "Bambalapitya", 6.893675316196351, 79.85541880130768);
		Center center6 = new Center(105L, "NBC", "NBC-01", "Galle Face", 6.922315722583153, 79.84599888324738);

		StockInfo stock = new StockInfo(1, "A+", "RCC", new Date(), 10, center1);
		StockInfo stock2 = new StockInfo(2, "B+", "PLATELET", new Date(), 25,
				center3);
		StockInfo stock3 = new StockInfo(3, "O+", "RCC", new Date(), 155,
				center4);
		StockInfo stock4 = new StockInfo(4, "AB+", "RCC", new Date(), 35,
				center1);
		StockInfo stock5 = new StockInfo(4, "AB+", "RCC", new Date(), 35,
				center1);
		StockInfo stock6 = new StockInfo(4, "A+", "RCC", new Date(), 65,
				center4);
		StockInfo stock7 = new StockInfo(4, "B+", "RCC", new Date(), 55,
				center4);
		StockInfo stock8 = new StockInfo(4, "O-", "RCC", new Date(), 45,
				center4);

		List stocks = new ArrayList();
		stocks.add(stock);
		stocks.add(stock2);
		stocks.add(stock3);
		stocks.add(stock4);
		stocks.add(stock5);
		stocks.add(stock6);
		stocks.add(stock7);
		stocks.add(stock8);
		stocks.add(center1);
		stocks.add(center2);
		stocks.add(center3);
		stocks.add(center4);
		stocks.add(center5);
		stocks.add(center6);

		return stocks;
	}
}
