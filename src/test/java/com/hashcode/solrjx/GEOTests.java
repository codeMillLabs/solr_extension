/*
 * Created on Oct 8, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.solrjx;

import com.hashcode.geo.fencing.Coordinate;
import com.hashcode.geo.fencing.GEOBoundary;

/**
 * <p>
 * GEOTests
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public class GEOTests {

	public static void main(String[] args) {
		testSimplePolygon();

		testPolygonWithHoles();

		testGEOFencingWithRealCoodrinates();
	}

	/**
	 * Create simple polygon and check that the point is inside
	 */
	public static void testSimplePolygon() {
		GEOBoundary polygon = GEOBoundary.Builder()
				.addVertex(new Coordinate(1, 3))
				.addVertex(new Coordinate(2, 8))
				.addVertex(new Coordinate(5, 4))
				.addVertex(new Coordinate(5, 9))
				.addVertex(new Coordinate(7, 5))
				.addVertex(new Coordinate(6, 1))
				.addVertex(new Coordinate(3, 1)).build();

		// Point is inside
		isInside(polygon, new Coordinate(5.5f, 7));

		// Point isn't inside
		isInside(polygon, new Coordinate(4.5f, 7));
	}

	/**
	 * Create polygon two holes and check that the point is inside
	 */
	public static void testPolygonWithHoles() {
		GEOBoundary polygon = GEOBoundary
				.Builder()
				.addVertex(new Coordinate(1, 2))
				// polygon
				.addVertex(new Coordinate(1, 6))
				.addVertex(new Coordinate(8, 7))
				.addVertex(new Coordinate(8, 1))
				.close()
				.addVertex(new Coordinate(2, 3))
				// hole one
				.addVertex(new Coordinate(5, 5))
				.addVertex(new Coordinate(6, 2)).close()
				.addVertex(new Coordinate(6, 6))
				// hole two
				.addVertex(new Coordinate(7, 6))
				.addVertex(new Coordinate(7, 5)).build();

		// Point is inside
		isInside(polygon, new Coordinate(6, 5));

		// Point isn't inside
		isInside(polygon, new Coordinate(4, 3));

		// Point isn't inside
		isInside(polygon, new Coordinate(6.5f, 5.8f));
	}

	/**
	 * Create polygon two holes and check that the point is inside
	 */
	public static void testGEOFencingWithRealCoodrinates() {
		System.out.println("testGEOFencingWithRealCoodrinates() :");
		// 6.850588962500949,79.87029433250427
		// 6.849289387650413,79.87381339073181
		// 6.852186795578171,79.87404942512512
		// 6.8523359263922, 79.87106680870056

		GEOBoundary polygon = GEOBoundary
				.Builder()
				// .addVertex(new Coordinate(6.850588962500949,
				// 79.87029433250427))
				// .addVertex(new Coordinate(6.849289387650413,
				// 79.87381339073181))
				// .addVertex(new Coordinate(6.852186795578171,
				// 79.87404942512512))
				// .addVertex(new Coordinate(6.8523359263922,
				// 79.87106680870056))

				// US Pentagon
				.addVertex(new Coordinate(-77.05788457660967, 38.87253259892824))
				.addVertex(new Coordinate(-77.05465973756702, 38.87291016281703))
				.addVertex(new Coordinate(-77.05315536854791, 38.87053267794386))
				.addVertex(new Coordinate(-77.05552622493516, 38.868757801256))
				.addVertex(new Coordinate(-77.05844056290393, 38.86996206506943))
				.addVertex(new Coordinate(-77.05788457660967, 38.87253259892824))
				.build();

		// Point is inside
		isInside(polygon, new Coordinate(6.850120263782773, 79.86881375312805));

		// Point isn't inside
		isInside(polygon, new Coordinate(-122.0856260178042, 37.42189285336048));

		// Point isn't inside
		isInside(polygon, new Coordinate(-77.05580139178142, 38.870832443487));
	}

	/**
	 * Check if point inside the polygon
	 * 
	 * @param polygon
	 * @param point
	 */
	private static void isInside(GEOBoundary polygon, Coordinate point) {
		boolean contains = polygon.contains(point);
		System.out.println("The point:" + point.toString() + " is "
				+ (contains ? "" : "not ") + "inside the polygon");
	}

}
