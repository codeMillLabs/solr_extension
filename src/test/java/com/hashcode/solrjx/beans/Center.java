/*
 * Created on Oct 3, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.solrjx.beans;

import java.util.ArrayList;
import java.util.List;

import com.hashcode.solrjx.Indexable;
import com.hashcode.solrjx.IndexableDataType;

/**
 * <p>
 * Model class for Center
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public class Center {

	@Indexable(fieldName = "id", id = true)
	private long id;

	@Indexable(fieldName = "center_name", type = IndexableDataType.STRING)
	private String name;

	@Indexable(fieldName = "location", type = IndexableDataType.STRING)
	private String locationId;

	@Indexable(fieldName = "officer_name", type = IndexableDataType.STRING)
	private String officerName;

	@Indexable(fieldName = "lat", type = IndexableDataType.COORDINATES)
	private double latitude;

	@Indexable(fieldName = "lng", type = IndexableDataType.COORDINATES)
	private double longitude;

	private List<StockInfo> stocks = new ArrayList<StockInfo>();

	public Center() {

	}

	public Center(long id, String name, String locationId, String officerName,
			double lat, double lng) {
		super();
		this.id = id;
		this.name = name;
		this.locationId = locationId;
		this.officerName = officerName;
		this.latitude = lat;
		this.longitude = lng;
	}

	/**
	 * <p>
	 * Indexing multi value stock in solr using stock id
	 * </p>
	 *
	 * @return List of Stock ids for indexing
	 */
	@Indexable(fieldName = "stocks", multivalue = true, type = IndexableDataType.STRING)
	public List<String> getStockIds() {
		List<String> ids = new ArrayList<String>();
		for (StockInfo stock : stocks) {

			ids.add("" + stock.getId());
		}
		return ids;
	}

	/**
	 * <p>
	 * Indexing multi value stock in solr using stock type
	 * </p>
	 *
	 * @return List of Stock ids for indexing
	 */
	@Indexable(fieldName = "stocks_type", multivalue = true, type = IndexableDataType.STRING)
	public List<String> getStockTypes() {
		List<String> types = new ArrayList<String>();
		for (StockInfo stock : stocks) {
			types.add(stock.getBloodType());
		}
		return types;
	}

	/**
	 * <p>
	 * Indexing center location for geo spacial search
	 * </p>
	 *
	 * @return location coordinates
	 */
	@Indexable(fieldName = "center_location", multivalue = false, type = IndexableDataType.LOCATION)
	public String getCoordinates() {
		return latitude + "," + longitude;
	}

	/**
	 * <p>
	 * Getter method for id
	 * </p>
	 * 
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * <p>
	 * Setter method for id
	 * </p>
	 *
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * <p>
	 * Getter method for name
	 * </p>
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * <p>
	 * Setter method for name
	 * </p>
	 *
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * <p>
	 * Getter method for locationId
	 * </p>
	 * 
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

	/**
	 * <p>
	 * Setter method for locationId
	 * </p>
	 *
	 * @param locationId
	 *            the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	/**
	 * <p>
	 * Getter method for officerName
	 * </p>
	 * 
	 * @return the officerName
	 */
	public String getOfficerName() {
		return officerName;
	}

	/**
	 * <p>
	 * Setter method for officerName
	 * </p>
	 *
	 * @param officerName
	 *            the officerName to set
	 */
	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}

	/**
	 * <p>
	 * Getter method for stocks
	 * </p>
	 * 
	 * @return the stocks
	 */
	public List<StockInfo> getStocks() {
		return stocks;
	}

	/**
	 * <p>
	 * Getter method for latitude
	 * </p>
	 * 
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * <p>
	 * Setter method for latitude
	 * </p>
	 *
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * <p>
	 * Getter method for longitude
	 * </p>
	 * 
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * <p>
	 * Setter method for longitude
	 * </p>
	 *
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
