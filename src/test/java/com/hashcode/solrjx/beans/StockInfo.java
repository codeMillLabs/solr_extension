/*
 * Created on Oct 3, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.solrjx.beans;

import java.util.Date;

import com.hashcode.solrjx.Indexable;
import com.hashcode.solrjx.IndexableDataType;

/**
 * <p>
 * Model class forStockInfo
 * </p>
 *
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 *
 */
public class StockInfo {

	@Indexable(fieldName = "id", id = true)
	private int id;

	@Indexable(fieldName = "blood_type", type = IndexableDataType.STRING)
	private String bloodType;

	@Indexable(fieldName = "sub_type", type = IndexableDataType.STRING)
	private String subType;

	@Indexable(fieldName = "donated_date", type = IndexableDataType.DATE)
	private Date donatedDate;

	@Indexable(fieldName = "stock_count", type = IndexableDataType.INT)
	private int count;

	private Center center;

	public StockInfo() {
	}

	public StockInfo(int id, String bloodType, String subType,
			Date donatedDate, int count, Center center) {
		super();
		this.id = id;
		this.bloodType = bloodType;
		this.subType = subType;
		this.donatedDate = donatedDate;
		this.count = count;

		if (center != null) {
			this.center = center;
			this.center.getStocks().add(this);
		}
	}

	/**
	 * <p>
	 * Get the center name to index in solr
	 * </p>
	 *
	 * @return center name
	 */
	@Indexable(fieldName = "center_name", type = IndexableDataType.STRING)
	public String getCenterName() {
		if (center != null) {
			return this.center.getName();
		}
		return null;
	}

	/**
	 * <p>
	 * Getter method for id
	 * </p>
	 * 
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * <p>
	 * Setter method for id
	 * </p>
	 *
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * <p>
	 * Getter method for bloodType
	 * </p>
	 * 
	 * @return the bloodType
	 */
	public String getBloodType() {
		return bloodType;
	}

	/**
	 * <p>
	 * Setter method for bloodType
	 * </p>
	 *
	 * @param bloodType
	 *            the bloodType to set
	 */
	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

	/**
	 * <p>
	 * Getter method for subType
	 * </p>
	 * 
	 * @return the subType
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * <p>
	 * Setter method for subType
	 * </p>
	 *
	 * @param subType
	 *            the subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * <p>
	 * Getter method for donatedDate
	 * </p>
	 * 
	 * @return the donatedDate
	 */
	public Date getDonatedDate() {
		return donatedDate;
	}

	/**
	 * <p>
	 * Setter method for donatedDate
	 * </p>
	 *
	 * @param donatedDate
	 *            the donatedDate to set
	 */
	public void setDonatedDate(Date donatedDate) {
		this.donatedDate = donatedDate;
	}

	/**
	 * <p>
	 * Getter method for center
	 * </p>
	 * 
	 * @return the center
	 */
	public Center getCenter() {
		return center;
	}

	/**
	 * <p>
	 * Setter method for center
	 * </p>
	 *
	 * @param center
	 *            the center to set
	 */
	public void setCenter(Center center) {
		this.center = center;
	}

	/**
	 * <p>
	 * Getter method for count
	 * </p>
	 * 
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * <p>
	 * Setter method for count
	 * </p>
	 *
	 * @param count
	 *            the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String
				.format("StockInfo [id=%s, bloodType=%s, subType=%s, donatedDate=%s, count=%s, center=%s]",
						id, bloodType, subType, donatedDate, count, center);
	}

}
